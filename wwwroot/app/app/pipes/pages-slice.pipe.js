"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var PagesSlicePipe = (function () {
    function PagesSlicePipe() {
        this.leftOffset = 5;
        this.rightOffset = 4;
    }
    PagesSlicePipe.prototype.transform = function (pages, currentPage) {
        var currentPageIndex = currentPage - 1, leftIndex = currentPageIndex - this.leftOffset, rightIndex = currentPageIndex + this.rightOffset;
        //find left side index
        if (leftIndex < 0) {
            rightIndex = rightIndex - leftIndex;
            leftIndex = 0;
        }
        //find right side index
        if (rightIndex >= pages.length) {
            //Try to count new left index, depends on new right index
            if (leftIndex - Math.abs(rightIndex - (pages.length - 1)) < 0) {
                leftIndex = 0;
            }
            else {
                leftIndex = leftIndex - Math.abs(rightIndex - (pages.length - 1));
            }
            rightIndex = pages.length - 1;
        }
        return pages.slice(leftIndex, rightIndex + 1);
    };
    PagesSlicePipe = __decorate([
        core_1.Pipe({
            name: 'pagesSlice'
        }), 
        __metadata('design:paramtypes', [])
    ], PagesSlicePipe);
    return PagesSlicePipe;
}());
exports.PagesSlicePipe = PagesSlicePipe;
//# sourceMappingURL=pages-slice.pipe.js.map