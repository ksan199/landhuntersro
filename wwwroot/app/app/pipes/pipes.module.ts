import { NgModule } from '@angular/core';

import { PagesSlicePipe }   from './pages-slice.pipe';

@NgModule({
    imports: [],
    exports: [PagesSlicePipe],
    declarations: [PagesSlicePipe],
    providers: [],
})
export class MyPipesModule {
    
}
