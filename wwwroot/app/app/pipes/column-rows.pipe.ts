﻿import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'columnRows'
})
export class ColumnRowsPipe implements PipeTransform {
    transform(array: any[], multiple: string) {
        let N = parseFloat(multiple);
        array = array.filter((value, index) => index % 3 == (0 + N));
        return array;
    }
}