﻿import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'pagesSlice'
})
export class PagesSlicePipe implements PipeTransform{
    private leftOffset = 5;
    private rightOffset = 4;

    transform(pages: number[], currentPage: number) {
        let currentPageIndex = currentPage - 1,
            leftIndex = currentPageIndex - this.leftOffset,
            rightIndex = currentPageIndex + this.rightOffset           

        //find left side index
        if (leftIndex < 0) {
            rightIndex = rightIndex - leftIndex;
            leftIndex = 0;
        }
        //find right side index
        if (rightIndex >= pages.length) {
            //Try to count new left index, depends on new right index
            if (leftIndex - Math.abs(rightIndex - (pages.length - 1)) < 0) {
                leftIndex = 0;
            }
            else {
                leftIndex = leftIndex - Math.abs(rightIndex - (pages.length - 1));
            }

            rightIndex = pages.length - 1;
        }

        return pages.slice(leftIndex, rightIndex + 1);
    }
}