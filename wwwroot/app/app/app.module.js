"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var http_1 = require('@angular/http');
var common_2 = require('@angular/common');
var app_component_1 = require('./app.component');
var app_routing_1 = require('./app.routing');
var mainpage_module_1 = require('./mainPage/mainpage.module');
var admin_module_1 = require('./admin/admin.module');
var auth_guard_service_1 = require('./services/auth-guard.service');
var auth_service_1 = require('./services/auth.service');
var login_component_1 = require('./login/login.component');
var pipes_module_1 = require('./pipes/pipes.module');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                mainpage_module_1.MainpageModule,
                common_1.CommonModule,
                admin_module_1.AdminModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                pipes_module_1.MyPipesModule,
                app_routing_1.routing
            ],
            declarations: [
                app_component_1.AppComp,
                login_component_1.LoginComp
            ],
            providers: [
                { provide: common_2.LocationStrategy, useClass: common_2.HashLocationStrategy },
                app_routing_1.appRoutingProviders,
                auth_guard_service_1.AuthGuard,
                auth_service_1.AuthService
            ],
            bootstrap: [
                app_component_1.AppComp
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map