"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var auth_service_1 = require('./../services/auth.service');
var LoginComp = (function () {
    function LoginComp(authService, router) {
        this.authService = authService;
        this.router = router;
        this.setMessage();
    }
    LoginComp.prototype.setMessage = function () {
        this.message = this.authService.isLoggedIn ? 'Выйти' : 'Войти';
    };
    LoginComp.prototype.login = function () {
        var _this = this;
        this.message = 'Trying to log in ...';
        this.authService.login(this.name, this.password).subscribe(function () {
            _this.setMessage();
            if (_this.authService.isLoggedIn) {
                //Get the redirect URL from our auth service
                //If no redirect has benn set, use the default
                var redirect = 'admin';
                //Redirect the user
                _this.router.navigate([redirect]);
            }
        }, function (error) {
            _this.message = 'Неверные логин/пароль, или мой код';
        });
    };
    LoginComp.prototype.logout = function () {
        this.authService.logout();
        this.setMessage();
    };
    LoginComp = __decorate([
        core_1.Component({
            selector: 'login',
            templateUrl: './app/app/login/login.component.html'
        }), 
        __metadata('design:paramtypes', [auth_service_1.AuthService, router_1.Router])
    ], LoginComp);
    return LoginComp;
}());
exports.LoginComp = LoginComp;
//# sourceMappingURL=login.component.js.map