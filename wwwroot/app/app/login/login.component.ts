﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth.service';

@Component({
    selector: 'login',
    templateUrl: './app/app/login/login.component.html'
})
export class LoginComp {
    message: string;
    name: string;
    password: string;

    constructor(public authService: AuthService, public router: Router) {
        this.setMessage();
    }

    setMessage() {
        this.message = this.authService.isLoggedIn ? 'Выйти' : 'Войти';
    }

    login() {
        this.message = 'Trying to log in ...';

        this.authService.login(this.name, this.password).subscribe(() => {
            this.setMessage();
            if (this.authService.isLoggedIn) {
                //Get the redirect URL from our auth service
                //If no redirect has benn set, use the default
                let redirect = 'admin';

                //Redirect the user
                this.router.navigate([redirect]);
            }
        }, error => {
            this.message = 'Неверные логин/пароль, или мой код';
        })
    }

    logout() {
        this.authService.logout();
        this.setMessage();
    }
}