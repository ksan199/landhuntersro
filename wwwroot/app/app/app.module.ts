﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AppComp } from './app.component';
import { routing, appRoutingProviders } from './app.routing';
import { MainComp } from './mainPage/mainpage.component';
import { MainpageModule } from './mainPage/mainpage.module';
import { AdminModule } from './admin/admin.module';
import { AuthGuard } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { LoginComp } from './login/login.component';
import { MyPipesModule } from './pipes/pipes.module';
import { PagesSlicePipe } from './pipes/pages-slice.pipe';

@NgModule({
    imports: [
        MainpageModule,
        CommonModule,
        AdminModule,
        FormsModule,
        HttpModule,
        MyPipesModule,
        routing
    ],
    declarations: [
        AppComp,
        LoginComp
    ],
    providers: [
        { provide: LocationStrategy, useClass: HashLocationStrategy },
        appRoutingProviders,
        AuthGuard,
        AuthService
    ],
    bootstrap: [
        AppComp
    ]
})
export class AppModule { }