﻿import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComp }              from './app.component';
import { MainComp } from './mainPage/mainpage.component';
import { RegistrationComp } from './registration/registration.component';
import { mainPageRouting } from './mainPage/mainpage.routing';
import { LoginComp } from './login/login.component';

const appRoutes: Routes = [
    {
        path: 'login',
        component: LoginComp
    }
];

export const appRoutingProviders: any[] = [
];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);