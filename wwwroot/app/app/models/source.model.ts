﻿let DEFAULT_NAME = "Source",
    DEFAULT_CATEGORY = "Category";

export class Source {
    name: string;
    category: string;

    constructor(name: string = DEFAULT_NAME, category: string = DEFAULT_CATEGORY) {
        this.name = name; this.category = category;
    }

    setName(name: string): Source {
        this.name = name;
        return this;
    }
    setCategory(category: string): Source {
        this.category = category;
        return this;
    }
}