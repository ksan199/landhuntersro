﻿let DEFAULT_IMG_URL = 'http://pics.v7.top.rbk.ru/v6_top_pics/resized/250xH/media/img/8/78/754720484104788.jpg';

export class ShortNews {
    id: string = '0';
    publishDate: string;
    shortMessage: string;
    imgUrl: string;
    state: string = 'inactive';
    prompt: boolean;

    constructor(publishDate: string = null, shortMessage: string = 'Avadakedavra', imgUrl: string = DEFAULT_IMG_URL) {
        this.publishDate = publishDate, this.shortMessage = shortMessage, this.imgUrl = imgUrl;
    }

    toggleState() {
        this.state = this.state == 'active' ? 'inactive' : 'active';
    }
}