"use strict";
var DEFAULT_TITLE = "TITLE", DEFAULT_MESSAGE = "BODY_MESSAGE", DEFAULT_PUBLISH_DATE = null, DEFAULT_IMG_URL = 'http://pics.v7.top.rbk.ru/v6_top_pics/resized/250xH/media/img/8/78/754720484104788.jpg', DEFAULT_SOURCE_URL = 'http://www.landhunter.ru';
var DetailNews = (function () {
    function DetailNews(title, message, publishDate, imgUrl, sourceUrl) {
        if (title === void 0) { title = DEFAULT_TITLE; }
        if (message === void 0) { message = DEFAULT_MESSAGE; }
        if (publishDate === void 0) { publishDate = DEFAULT_PUBLISH_DATE; }
        if (imgUrl === void 0) { imgUrl = DEFAULT_IMG_URL; }
        if (sourceUrl === void 0) { sourceUrl = DEFAULT_SOURCE_URL; }
        this.id = '0';
        this.state = 'inactive';
        this.title = title;
        this.message = message;
        this.publishDate = publishDate;
        this.imgUrl = imgUrl, this.sourceUrl = sourceUrl;
    }
    return DetailNews;
}());
exports.DetailNews = DetailNews;
//# sourceMappingURL=detail-news.model.js.map