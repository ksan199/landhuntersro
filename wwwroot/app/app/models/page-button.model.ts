﻿export let SELECTED_STATE = 'selected',
    NOT_SELECTED_STATE = 'notSelected',
    CURRENT_STATE = 'current';

export class PageButton {
    number: string | number;
    state = NOT_SELECTED_STATE;

    constructor(number: string | number) {
        this.number = number;
    }

    toggleState(newState: string) {
        if (this.state == CURRENT_STATE) {
            return;
        }
        this.state = newState;
    }
}