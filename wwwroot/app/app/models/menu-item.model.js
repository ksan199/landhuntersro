"use strict";
var DEFAULT_NAME = "Menu Button";
var MenuItem = (function () {
    function MenuItem(category) {
        this.name = DEFAULT_NAME;
        this.category = category;
    }
    MenuItem.prototype.setName = function (name) {
        this.name = name;
        return this;
    };
    return MenuItem;
}());
exports.MenuItem = MenuItem;
//# sourceMappingURL=menu-item.model.js.map