"use strict";
var DEFAULT_NAME = "Source", DEFAULT_CATEGORY = "Category";
var Source = (function () {
    function Source(name, category) {
        if (name === void 0) { name = DEFAULT_NAME; }
        if (category === void 0) { category = DEFAULT_CATEGORY; }
        this.name = name;
        this.category = category;
    }
    Source.prototype.setName = function (name) {
        this.name = name;
        return this;
    };
    Source.prototype.setCategory = function (category) {
        this.category = category;
        return this;
    };
    return Source;
}());
exports.Source = Source;
//# sourceMappingURL=source.model.js.map