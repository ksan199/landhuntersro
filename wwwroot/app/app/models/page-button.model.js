"use strict";
exports.SELECTED_STATE = 'selected', exports.NOT_SELECTED_STATE = 'notSelected', exports.CURRENT_STATE = 'current';
var PageButton = (function () {
    function PageButton(number) {
        this.state = exports.NOT_SELECTED_STATE;
        this.number = number;
    }
    PageButton.prototype.toggleState = function (newState) {
        if (this.state == exports.CURRENT_STATE) {
            return;
        }
        this.state = newState;
    };
    return PageButton;
}());
exports.PageButton = PageButton;
//# sourceMappingURL=page-button.model.js.map