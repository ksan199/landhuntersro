﻿let DEFAULT_TITLE = "TITLE",
    DEFAULT_MESSAGE = "BODY_MESSAGE",
    DEFAULT_PUBLISH_DATE = null,
    DEFAULT_IMG_URL = 'http://pics.v7.top.rbk.ru/v6_top_pics/resized/250xH/media/img/8/78/754720484104788.jpg',
    DEFAULT_SOURCE_URL = 'http://www.landhunter.ru'

export class DetailNews {
    id: string = '0'
    title: string;
    message: string;
    publishDate: string;
    imgUrl: string;
    state: string = 'inactive';
    sourceUrl: string;
    sourceName: string;

    constructor(title: string = DEFAULT_TITLE, message: string = DEFAULT_MESSAGE, publishDate: string = DEFAULT_PUBLISH_DATE, imgUrl: string = DEFAULT_IMG_URL, sourceUrl: string = DEFAULT_SOURCE_URL) {
        this.title = title; this.message = message; this.publishDate = publishDate; this.imgUrl = imgUrl, this.sourceUrl = sourceUrl;
    }  
}