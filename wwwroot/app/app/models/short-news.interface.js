"use strict";
var DEFAULT_IMG_URL = 'http://pics.v7.top.rbk.ru/v6_top_pics/resized/250xH/media/img/8/78/754720484104788.jpg';
var ShortNews = (function () {
    function ShortNews(publishDate, shortMessage, imgUrl) {
        if (publishDate === void 0) { publishDate = null; }
        if (shortMessage === void 0) { shortMessage = 'Avadakedavra'; }
        if (imgUrl === void 0) { imgUrl = DEFAULT_IMG_URL; }
        this.id = '0';
        this.state = 'inactive';
        this.publishDate = publishDate, this.shortMessage = shortMessage, this.imgUrl = imgUrl;
    }
    ShortNews.prototype.toggleState = function () {
        this.state = this.state == 'active' ? 'inactive' : 'active';
    };
    return ShortNews;
}());
exports.ShortNews = ShortNews;
//# sourceMappingURL=short-news.interface.js.map