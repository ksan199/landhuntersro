﻿let DEFAULT_NAME = "Menu Button"

export class MenuItem {
    name = DEFAULT_NAME;
    category: string;

    constructor(category: string) {
        this.category = category;
    }

    setName(name: string): MenuItem {
        this.name = name;
        return this;
    }
}