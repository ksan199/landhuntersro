﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { MenuItem } from './../models/menu-item.model';
import { AuthService } from './../services/auth.service';

@Component({
    selector: 'admin',
    templateUrl: './app/app/admin/admin.component.html'
})
export class AdminComp {
    menuItems: MenuItem[] = [
        new MenuItem('prompt').setName('Главная'),
        new MenuItem('all').setName('Новости')
    ]

    constructor(
        private authService: AuthService,
        private router: Router
    ) {}

    logout(): void {
        this.authService.logout()
            .subscribe(success => {
                this.router.navigate(['']);
            });
    }
}