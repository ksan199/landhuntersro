﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { adminRouting } from './admin.routing';
import { AdminComp } from './admin.component';
import { AuthGuard } from './../services/auth-guard.service';
import { AuthService } from './../services/auth.service';
import { AllNewsComp } from './all-news/all-news.component';
import { MyPipesModule } from './../pipes/pipes.module';

@NgModule({
    imports: [
        adminRouting,
        BrowserModule,
        RouterModule,
        FormsModule,
        MyPipesModule
    ],
    declarations: [
        AdminComp,
        AllNewsComp
    ],
    providers: [
        
    ]
})
export class AdminModule { }
