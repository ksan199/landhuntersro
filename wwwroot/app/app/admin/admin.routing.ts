﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComp } from './admin.component';
import { AuthGuard } from './../services/auth-guard.service';
import { AllNewsComp } from './all-news/all-news.component';

const adminRoutes: Routes = [
    {
        path: 'admin',
        component: AdminComp,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'all/1'
            },
            {
                path: ':category',
                pathMatch: 'full',
                redirectTo: ':category/1'
            },
            {
                path: ':category/:page',
                component: AllNewsComp
            }
        ]
    }
];

export const adminRouting: ModuleWithProviders = RouterModule.forChild(adminRoutes);