"use strict";
var router_1 = require('@angular/router');
var admin_component_1 = require('./admin.component');
var auth_guard_service_1 = require('./../services/auth-guard.service');
var all_news_component_1 = require('./all-news/all-news.component');
var adminRoutes = [
    {
        path: 'admin',
        component: admin_component_1.AdminComp,
        canActivate: [auth_guard_service_1.AuthGuard],
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'all/1'
            },
            {
                path: ':category',
                pathMatch: 'full',
                redirectTo: ':category/1'
            },
            {
                path: ':category/:page',
                component: all_news_component_1.AllNewsComp
            }
        ]
    }
];
exports.adminRouting = router_1.RouterModule.forChild(adminRoutes);
//# sourceMappingURL=admin.routing.js.map