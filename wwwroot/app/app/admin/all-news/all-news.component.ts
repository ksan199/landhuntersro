﻿import { 
    Component,
    trigger,
    state,
    style,
    transition,
    animate
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ShortNews } from './../../models/short-news.interface';
import { NewsService } from './../../services/news.service';
import { PageButton, CURRENT_STATE, NOT_SELECTED_STATE, SELECTED_STATE } from './../../models/page-button.model';

@Component({
    selector: 'admin-all-news',
    templateUrl: './app/app/admin/all-news/all-news.component.html',
    animations: [
        trigger('newsState', [
            state('inactive', style({
                backgroundColor: '#f3f2ef',
                // transform: 'scale(1)'
            })),
            state('active', style({
                backgroundColor: '#cfd8dc',
                // transform: 'scale(1.005)'
            })),
            transition('inactive => active', animate('100ms ease-in')),
            transition('active => inactive', animate('100ms ease-out'))
        ]),
        trigger('pageState', [
            state('notSelected', style({
                transform: 'scale(1)'
            })),
            state('selected', style({
                transform: 'scale(1.5)',
                backgroundColor: '#fff'
            })),
            state('current', style({
                transform: 'scale(1.5)',
                backgroundColor: '#cfd8dc',
                zIndex: '2'
            })),
            transition('notSelected <=> selected, notSelected <=> current, current <=> selected', animate('100ms ease'))
        ])
    ]
})
export class AllNewsComp {
    newsList: ShortNews[];
    category: string;
    page: string;
    arrayOfPages: PageButton[] = [];
    currentPageRef: PageButton;

    constructor(
        private newsService: NewsService,
        private router: Router,
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        this._resolveNews();
    }

    togglePrompt(news: ShortNews) {
        if (news.prompt != true) {
            this.newsService.setAsPrompt(news)
                .subscribe(result => {
                    news.prompt = true;
                }, error => {
                    console.log(error);
                });            
        }
        else {
            this.newsService.resetPrompt(news)
                .subscribe(result => {
                    news.prompt = false;
                }, error => {
                    console.log(error);
                });
        }
    }

    nextPage() {
        let nextPageNumber = parseFloat(this.page) + 1;
        nextPageNumber = nextPageNumber > this.arrayOfPages.length ? this.arrayOfPages.length : nextPageNumber;

        this.setNewCurrentPage(this.arrayOfPages[nextPageNumber - 1]);

        this.router.navigate(['/admin', this.category, nextPageNumber]);
    }

    /**
     * Navigates to a previous page relative to the current one
     */
    prevPage() {
        let prevPageNumber = parseFloat(this.page) - 1;
        prevPageNumber = prevPageNumber < 1 ? 1 : prevPageNumber;
        
        this.setNewCurrentPage(this.arrayOfPages[prevPageNumber - 1]);

        this.router.navigate(['/admin', this.category, prevPageNumber]);
    }

    /**
     * Sets state for first page
     */
    _setFirstPage(): void {
        try {
            this.currentPageRef = this.arrayOfPages[parseFloat(this.page) - 1];
            this.currentPageRef.state = CURRENT_STATE;
        }
        catch (e){

        }
    }

    /**
     * Sets state for given page button, also removes state for old one
     * @param clickedPage
     */
    setNewCurrentPage(clickedPage: PageButton) {
        if (clickedPage == this.currentPageRef) return;
        //Change old page's state
        this.currentPageRef.state = NOT_SELECTED_STATE;
        clickedPage.state = CURRENT_STATE;
        this.currentPageRef = clickedPage;
    }

    _resolveNews() {
        this.route.params
            .switchMap(params => {
                this.page = params['page'];
                this.category = params['category'];
                return this.newsService.getNews(this.category, this.page);
            })
            .switchMap(result => {
                if (result == null) {
                    this.newsList = [];
                }
                this.newsList = result;
                return this.newsService.getTotalPages(this.category);
            })
            .subscribe(totalPages =>{
                if (totalPages == null) {
                    return;
                }

                //Set array of pages for particular category
                if (this.arrayOfPages.length == totalPages) {
                    return;
                }
                let newPages = [];
                for (let i = 0; i < totalPages; i++) {
                    newPages.push(new PageButton(i + 1));
                }
                this.arrayOfPages = newPages;
                this._setFirstPage();
            });
    }
}

