﻿import { Component } from '@angular/core';

import { Source } from './../models/source.model';
import { DateService } from './../services/date.service';
import { AuthService } from './../services/auth.service';

@Component({
    selector: 'main',
    templateUrl: './app/app/mainPage/mainpage.component.html'
})
export class MainComp {
    sourceList: Source[] = [
        new Source().setName("Главные новости").setCategory("prompt"),
        new Source().setName("Все новости").setCategory("all"),
        new Source().setName("РБК").setCategory("rbc_ru"),
        new Source().setName("Росреестр").setCategory("rosreestr_ru_press"),
        new Source().setName("РИА").setCategory("riarealty_ru"),
        new Source().setName("Яндекс").setCategory("zakupki_gov_ru")
    ];

    currentDate = "";

    constructor(
        private dateService: DateService,
        private authService: AuthService
    ) { }

    ngOnInit() {
        this.currentDate = this.dateService.getCurrentTime();
    }
}
