﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsListComp } from './news-list/news-list.component';
import { MainComp } from './mainpage.component';
import { NewsDetailComp } from './news-detail/news-detail.component';
import { NewsListResolve } from './../services/news-list-resolve.service';
import { NewsListErrorComp } from './news-list/news-list-error.component';

const mainPageRoutes: Routes = [
    {
        path: '',
        redirectTo: 'main',
        pathMatch: 'full'
    },
    {
        path: 'main',
        component: MainComp,
        children: [
            { path: '', redirectTo: 'prompt/1', pathMatch: 'full' },
            { path: ':category', redirectTo: ':category/1', pathMatch: 'full' },
            { path: ':category/error', component: NewsListErrorComp },
            { path: ':category/:id', component: NewsListComp },            
            { path: ':category/detail/:id', component: NewsDetailComp }            
        ]
    }
];

export const mainPageRouting: ModuleWithProviders = RouterModule.forChild(mainPageRoutes);