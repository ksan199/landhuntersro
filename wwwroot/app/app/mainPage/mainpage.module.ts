﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { MainComp } from './mainpage.component';
import { mainPageRouting } from './mainpage.routing';
import { NewsListComp } from './news-list/news-list.component';
import { NewsDetailComp } from './news-detail/news-detail.component';
import { NewsService } from './../services/news.service';
import { NewsListResolve } from './../services/news-list-resolve.service';
import { ColumnRowsPipe } from './../pipes/column-rows.pipe';
import { PagesSlicePipe } from './../pipes/pages-slice.pipe';
import { MyPipesModule } from './../pipes/pipes.module';
import { NewsListErrorComp } from './news-list/news-list-error.component';
import { DateService } from './../services/date.service';

@NgModule({
    imports: [
        mainPageRouting,
        BrowserModule,
        FormsModule,
        MyPipesModule
    ],
    declarations: [
        MainComp,
        NewsListComp,
        NewsDetailComp,
        ColumnRowsPipe,
        NewsListErrorComp,
    ],
    providers: [
        NewsService,
        NewsListResolve,
        DateService
    ]
})
export class MainpageModule { }