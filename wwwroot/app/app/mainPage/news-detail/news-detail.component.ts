﻿import {
    Component,
    trigger,
    transition,
    state,
    style,
    animate
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NewsService } from './../../services/news.service';
import { DetailNews } from './../../models/detail-news.model';
import 'rxjs/add/operator/switchMap';

declare let Ya: any;

@Component({
    selector: 'news-detail',
    templateUrl: './app/app/mainPage/news-detail/news-detail.component.html',
    animations: [
        trigger('backButtonState', [
            state('active', style({
                transform: 'scale(1.025)',
                //boxShadow: '0 0 1px rgba(0, 0, 0, .5)'
            })),
            state('inactive', style({
                transform: 'scale(1)',
                //boxShadow: 'none'
            })),
            transition('active <=> inactive', animate('10ms ease-in'))
        ])
    ]
})
export class NewsDetailComp {
    id: string;
    newsText: DetailNews = new DetailNews("HELLO_WORLD", "HI THERE");
    backButtonState = 'inactive';

    constructor(private route: ActivatedRoute, private newsService: NewsService) { }

    ngOnInit() {
        this.initSocialWebButtons();

        let scope = this;
        //Updates news details
        this.route.params.switchMap(params => {
            this.id = params['id'];
            return this.newsService.getNewsDetails(this.id)
        }).subscribe(result => {
            scope.newsText = result;
            }, error => {
                console.log(error);
            }
        );
    }

    goBack(): void {
        window.history.back();
    }

    toggleBackButtonState(): void {
        this.backButtonState = this.backButtonState == 'active' ? 'inactive' : 'active';
    }

    initSocialWebButtons(): void {
        Ya.share2('share-buttons', {
            theme: {
                services: 'vkontakte,facebook,twitter,gplus'
            }
        });
    }
}