"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var news_service_1 = require('./../../services/news.service');
var detail_news_model_1 = require('./../../models/detail-news.model');
require('rxjs/add/operator/switchMap');
var NewsDetailComp = (function () {
    function NewsDetailComp(route, newsService) {
        this.route = route;
        this.newsService = newsService;
        this.newsText = new detail_news_model_1.DetailNews("HELLO_WORLD", "HI THERE");
        this.backButtonState = 'inactive';
    }
    NewsDetailComp.prototype.ngOnInit = function () {
        var _this = this;
        this.initSocialWebButtons();
        var scope = this;
        //Updates news details
        this.route.params.switchMap(function (params) {
            _this.id = params['id'];
            return _this.newsService.getNewsDetails(_this.id);
        }).subscribe(function (result) {
            scope.newsText = result;
        }, function (error) {
            console.log(error);
        });
    };
    NewsDetailComp.prototype.goBack = function () {
        window.history.back();
    };
    NewsDetailComp.prototype.toggleBackButtonState = function () {
        this.backButtonState = this.backButtonState == 'active' ? 'inactive' : 'active';
    };
    NewsDetailComp.prototype.initSocialWebButtons = function () {
        Ya.share2('share-buttons', {
            theme: {
                services: 'vkontakte,facebook,twitter,gplus'
            }
        });
    };
    NewsDetailComp = __decorate([
        core_1.Component({
            selector: 'news-detail',
            templateUrl: './app/app/mainPage/news-detail/news-detail.component.html',
            animations: [
                core_1.trigger('backButtonState', [
                    core_1.state('active', core_1.style({
                        transform: 'scale(1.025)',
                    })),
                    core_1.state('inactive', core_1.style({
                        transform: 'scale(1)',
                    })),
                    core_1.transition('active <=> inactive', core_1.animate('10ms ease-in'))
                ])
            ]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, news_service_1.NewsService])
    ], NewsDetailComp);
    return NewsDetailComp;
}());
exports.NewsDetailComp = NewsDetailComp;
//# sourceMappingURL=news-detail.component.js.map