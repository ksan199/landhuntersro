"use strict";
var router_1 = require('@angular/router');
var news_list_component_1 = require('./news-list/news-list.component');
var mainpage_component_1 = require('./mainpage.component');
var news_detail_component_1 = require('./news-detail/news-detail.component');
var news_list_error_component_1 = require('./news-list/news-list-error.component');
var mainPageRoutes = [
    {
        path: '',
        redirectTo: 'main',
        pathMatch: 'full'
    },
    {
        path: 'main',
        component: mainpage_component_1.MainComp,
        children: [
            { path: '', redirectTo: 'prompt/1', pathMatch: 'full' },
            { path: ':category', redirectTo: ':category/1', pathMatch: 'full' },
            { path: ':category/error', component: news_list_error_component_1.NewsListErrorComp },
            { path: ':category/:id', component: news_list_component_1.NewsListComp },
            { path: ':category/detail/:id', component: news_detail_component_1.NewsDetailComp }
        ]
    }
];
exports.mainPageRouting = router_1.RouterModule.forChild(mainPageRoutes);
//# sourceMappingURL=mainpage.routing.js.map