"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var source_model_1 = require('./../models/source.model');
var date_service_1 = require('./../services/date.service');
var auth_service_1 = require('./../services/auth.service');
var MainComp = (function () {
    function MainComp(dateService, authService) {
        this.dateService = dateService;
        this.authService = authService;
        this.sourceList = [
            new source_model_1.Source().setName("Главные новости").setCategory("prompt"),
            new source_model_1.Source().setName("Все новости").setCategory("all"),
            new source_model_1.Source().setName("РБК").setCategory("rbc_ru"),
            new source_model_1.Source().setName("Росреестр").setCategory("rosreestr_ru_press"),
            new source_model_1.Source().setName("РИА").setCategory("riarealty_ru"),
            new source_model_1.Source().setName("Яндекс").setCategory("zakupki_gov_ru")
        ];
        this.currentDate = "";
    }
    MainComp.prototype.ngOnInit = function () {
        this.currentDate = this.dateService.getCurrentTime();
    };
    MainComp = __decorate([
        core_1.Component({
            selector: 'main',
            templateUrl: './app/app/mainPage/mainpage.component.html'
        }), 
        __metadata('design:paramtypes', [date_service_1.DateService, auth_service_1.AuthService])
    ], MainComp);
    return MainComp;
}());
exports.MainComp = MainComp;
//# sourceMappingURL=mainpage.component.js.map