"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var mainpage_component_1 = require('./mainpage.component');
var mainpage_routing_1 = require('./mainpage.routing');
var news_list_component_1 = require('./news-list/news-list.component');
var news_detail_component_1 = require('./news-detail/news-detail.component');
var news_service_1 = require('./../services/news.service');
var news_list_resolve_service_1 = require('./../services/news-list-resolve.service');
var column_rows_pipe_1 = require('./../pipes/column-rows.pipe');
var pipes_module_1 = require('./../pipes/pipes.module');
var news_list_error_component_1 = require('./news-list/news-list-error.component');
var date_service_1 = require('./../services/date.service');
var MainpageModule = (function () {
    function MainpageModule() {
    }
    MainpageModule = __decorate([
        core_1.NgModule({
            imports: [
                mainpage_routing_1.mainPageRouting,
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                pipes_module_1.MyPipesModule
            ],
            declarations: [
                mainpage_component_1.MainComp,
                news_list_component_1.NewsListComp,
                news_detail_component_1.NewsDetailComp,
                column_rows_pipe_1.ColumnRowsPipe,
                news_list_error_component_1.NewsListErrorComp,
            ],
            providers: [
                news_service_1.NewsService,
                news_list_resolve_service_1.NewsListResolve,
                date_service_1.DateService
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], MainpageModule);
    return MainpageModule;
}());
exports.MainpageModule = MainpageModule;
//# sourceMappingURL=mainpage.module.js.map