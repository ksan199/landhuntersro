"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var news_service_1 = require('./../../services/news.service');
require('rxjs/add/operator/map');
require('rxjs/add/operator/switchMap');
var page_button_model_1 = require('./../../models/page-button.model');
var NewsListComp = (function () {
    function NewsListComp(route, router, newsService) {
        this.route = route;
        this.router = router;
        this.newsService = newsService;
        this.newsList = [];
        this.numberOfNewsColumns = Array(3).fill(0);
        this.arrayOfPages = [];
        this.currentPage = '0';
    }
    NewsListComp.prototype.ngOnInit = function () {
        this._resolveNews();
    };
    /**
     * Navigates to a next page relative to the current one
     */
    NewsListComp.prototype.nextPage = function () {
        var nextPageNumber = parseFloat(this.currentPage) + 1;
        nextPageNumber = nextPageNumber > this.arrayOfPages.length ? this.arrayOfPages.length : nextPageNumber;
        this.setNewCurrentPage(this.arrayOfPages[nextPageNumber - 1]);
        this.router.navigateByUrl('main/' + this.category + '/' + nextPageNumber);
    };
    /**
     * Navigates to a previous page relative to the current one
     */
    NewsListComp.prototype.prevPage = function () {
        var prevPageNumber = parseFloat(this.currentPage) - 1;
        prevPageNumber = prevPageNumber < 1 ? 1 : prevPageNumber;
        this.setNewCurrentPage(this.arrayOfPages[prevPageNumber - 1]);
        this.router.navigateByUrl('main/' + this.category + '/' + prevPageNumber);
    };
    NewsListComp.prototype._resolveNews = function () {
        var _this = this;
        var $scope = this;
        this.route.params.subscribe(function (params) {
            _this.category = params['category'];
        });
        this.route.params
            .map(function (params) { return params['id']; })
            .switchMap(function (id) {
            _this.currentPage = id;
            return _this.newsService.getNews(_this.category, _this.currentPage);
        })
            .switchMap(function (result) {
            if (result == null) {
                _this.router.navigateByUrl('main/' + _this.category + '/error');
            }
            _this.newsList = result;
            return _this.newsService.getTotalPages(_this.category);
        }).subscribe(function (totalPages) {
            if (totalPages == null) {
                return;
            }
            //Set array of pages for particular category
            if (_this.arrayOfPages.length == totalPages) {
                return;
            }
            var newPages = [];
            for (var i = 0; i < totalPages; i++) {
                newPages.push(new page_button_model_1.PageButton(i + 1));
            }
            _this.arrayOfPages = newPages;
            _this._setFirstPage();
        });
    };
    /**
     * Sets state for first page
     */
    NewsListComp.prototype._setFirstPage = function () {
        try {
            this.currentPageRef = this.arrayOfPages[parseFloat(this.currentPage) - 1];
            this.currentPageRef.state = page_button_model_1.CURRENT_STATE;
        }
        catch (error) {
        }
    };
    /**
     * Sets state for given page button, also removes state for old one
     * @param clickedPage
     */
    NewsListComp.prototype.setNewCurrentPage = function (clickedPage) {
        if (clickedPage == this.currentPageRef)
            return;
        //Change old page's state
        this.currentPageRef.state = page_button_model_1.NOT_SELECTED_STATE;
        clickedPage.state = page_button_model_1.CURRENT_STATE;
        this.currentPageRef = clickedPage;
    };
    NewsListComp = __decorate([
        core_1.Component({
            selector: 'news-list',
            templateUrl: './app/app/mainPage/news-list/news-list.component.html',
            animations: [
                core_1.trigger('newsState', [
                    core_1.state('inactive', core_1.style({
                        backgroundColor: '#fff',
                        transform: 'scale(1)'
                    })),
                    core_1.state('active', core_1.style({
                        backgroundColor: '#cfd8dc',
                        transform: 'scale(1.025)'
                    })),
                    core_1.transition('inactive => active', core_1.animate('100ms ease-in')),
                    core_1.transition('active => inactive', core_1.animate('100ms ease-out'))
                ]),
                core_1.trigger('pageState', [
                    core_1.state('notSelected', core_1.style({
                        transform: 'scale(1)'
                    })),
                    core_1.state('selected', core_1.style({
                        transform: 'scale(1.5)',
                        backgroundColor: '#f3f2ef'
                    })),
                    core_1.state('current', core_1.style({
                        transform: 'scale(1.5)',
                        backgroundColor: '#cfd8dc',
                        zIndex: '2'
                    })),
                    core_1.transition('notSelected <=> selected, notSelected <=> current, current <=> selected', core_1.animate('100ms ease'))
                ])
            ]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, news_service_1.NewsService])
    ], NewsListComp);
    return NewsListComp;
}());
exports.NewsListComp = NewsListComp;
//# sourceMappingURL=news-list.component.js.map