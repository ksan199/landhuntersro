﻿import {
    Component,
    trigger,
    state,
    style,
    transition,
    animate
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ShortNews } from './../../models/short-news.interface';
import { NewsService } from './../../services/news.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { PageButton, CURRENT_STATE, NOT_SELECTED_STATE, SELECTED_STATE } from './../../models/page-button.model';


@Component({
    selector: 'news-list',
    templateUrl: './app/app/mainPage/news-list/news-list.component.html',
    animations: [
        trigger('newsState', [
            state('inactive', style({
                backgroundColor: '#fff',
                transform: 'scale(1)'
            })),
            state('active', style({
                backgroundColor: '#cfd8dc',
                transform: 'scale(1.025)'
            })),
            transition('inactive => active', animate('100ms ease-in')),
            transition('active => inactive', animate('100ms ease-out'))
        ]),
        trigger('pageState', [
            state('notSelected', style({
                transform: 'scale(1)'
            })),
            state('selected', style({
                transform: 'scale(1.5)',
                backgroundColor: '#f3f2ef'
            })),
            state('current', style({
                transform: 'scale(1.5)',
                backgroundColor: '#cfd8dc',
                zIndex: '2'
            })),
            transition('notSelected <=> selected, notSelected <=> current, current <=> selected', animate('100ms ease'))
        ])
    ]
})
export class NewsListComp {
    newsList: ShortNews[] = [];
    category: string;

    numberOfNewsColumns = Array(3).fill(0);
    arrayOfPages: PageButton[] = [];
    currentPage: string = '0';
    currentPageRef: PageButton;

 

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private newsService: NewsService
    ) { }

    ngOnInit() {
        this._resolveNews();
    }

    /**
     * Navigates to a next page relative to the current one
     */
    nextPage() {
        let nextPageNumber = parseFloat(this.currentPage) + 1;
        nextPageNumber = nextPageNumber > this.arrayOfPages.length ? this.arrayOfPages.length : nextPageNumber;

        this.setNewCurrentPage(this.arrayOfPages[nextPageNumber - 1]);

        this.router.navigateByUrl('main/' + this.category + '/' + nextPageNumber);
    }

    /**
     * Navigates to a previous page relative to the current one
     */
    prevPage() {
        let prevPageNumber = parseFloat(this.currentPage) - 1;
        prevPageNumber = prevPageNumber < 1 ? 1 : prevPageNumber;
        
        this.setNewCurrentPage(this.arrayOfPages[prevPageNumber - 1]);

        this.router.navigateByUrl('main/' + this.category + '/' + prevPageNumber);
    }

    _resolveNews() {
        let $scope = this;
        this.route.params.subscribe(params => {
            this.category = params['category'];
        });

        this.route.params
            .map(params => params['id'])
            .switchMap(id => {
                this.currentPage = id;
                return this.newsService.getNews(this.category, this.currentPage);
            })
            .switchMap(result => {
                if (result == null) {
                    this.router.navigateByUrl('main/' + this.category + '/error');
                }
                this.newsList = result;
                return this.newsService.getTotalPages(this.category);
            }).subscribe(totalPages => {
                if (totalPages == null) {
                    return;
                }

                //Set array of pages for particular category
                if (this.arrayOfPages.length == totalPages) {
                    return;
                }
                let newPages = [];
                for (let i = 0; i < totalPages; i++) {
                    newPages.push(new PageButton(i + 1));
                }
                this.arrayOfPages = newPages;
                this._setFirstPage();
            });
    }

    /**
     * Sets state for first page
     */
    _setFirstPage(): void {
        try {
            this.currentPageRef = this.arrayOfPages[parseFloat(this.currentPage) - 1];
            this.currentPageRef.state = CURRENT_STATE;
        } catch (error) {       
        }
        
    }

    /**
     * Sets state for given page button, also removes state for old one
     * @param clickedPage
     */
    setNewCurrentPage(clickedPage: PageButton) {
        if (clickedPage == this.currentPageRef) return;
        //Change old page's state
        this.currentPageRef.state = NOT_SELECTED_STATE;
        clickedPage.state = CURRENT_STATE;
        this.currentPageRef = clickedPage;
    }
}