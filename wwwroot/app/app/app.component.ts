﻿import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
    selector: 'my-app',
    templateUrl: './app/app/app.component.html'
})
export class AppComp {
    constructor(
        private authService: AuthService
    ) {}

    ngOnInit() {
        this.authService.CheckLoginStatus().subscribe();
    }
}