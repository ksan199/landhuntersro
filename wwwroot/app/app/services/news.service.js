"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/observable/of');
require('rxjs/add/operator/do');
require('rxjs/add/operator/delay');
var detail_news_model_1 = require('./../models/detail-news.model');
var short_news_interface_1 = require('./../models/short-news.interface');
var NewsService = (function () {
    function NewsService(http) {
        this.http = http;
        this.apiGetNews = '/api/news/get';
        this.apiGetTotalPages = '/api/news/gettotalpages';
        this.apiPromptStatus = '/api/news/promptstatus';
        this.apiGetDetails = '/api/news/details';
    }
    NewsService.prototype.getNewsDetails = function (id) {
        var _this = this;
        var qstring = new http_1.URLSearchParams();
        qstring.append('id', id);
        return this.http.get(this.apiGetDetails, { search: qstring })
            .map(function (result) {
            var rawDetails = result.json();
            var details = new detail_news_model_1.DetailNews();
            details.id = rawDetails.id;
            if (!rawDetails.imgUrl || rawDetails.imgUrl == '') {
                details.imgUrl = null;
            }
            else if (rawDetails.imgUrl.startsWith('/')) {
                details.imgUrl = _this._correctUrl(rawDetails.sourceUrl, rawDetails.imgUrl);
            }
            else {
                details.imgUrl = rawDetails.imgUrl;
            }
            details.message = rawDetails.message;
            details.publishDate = rawDetails.publishDate;
            details.sourceUrl = rawDetails.sourceUrl;
            details.title = rawDetails.title;
            details.sourceName = rawDetails.sourceName;
            return details;
        });
    };
    /**
     * Returns a list of short news for current category and page
     */
    NewsService.prototype.getNews = function (category, page, prompt) {
        var _this = this;
        if (prompt === void 0) { prompt = false; }
        var qstring = new http_1.URLSearchParams();
        qstring.append('category', category);
        qstring.append('page', page);
        return this.http.get(this.apiGetNews, { search: qstring })
            .map(function (result) {
            var rawNews = result.json();
            var news = [];
            for (var _i = 0, rawNews_1 = rawNews; _i < rawNews_1.length; _i++) {
                var i = rawNews_1[_i];
                var toPush = new short_news_interface_1.ShortNews();
                toPush.id = i.id;
                if (!i.imgUrl || i.imgUrl == '') {
                    toPush.imgUrl = null;
                }
                else if (i.imgUrl.startsWith('/')) {
                    toPush.imgUrl = _this._correctUrl(i.originalUrl, i.imgUrl);
                }
                else {
                    toPush.imgUrl = i.imgUrl;
                }
                if (i.prompt == null || (i.prompt == 'false')) {
                    toPush.prompt = false;
                }
                else {
                    toPush.prompt = true;
                }
                toPush.publishDate = i.publishDate;
                if (i.shortMessage != null)
                    toPush.shortMessage = i.shortMessage;
                news.push(toPush);
            }
            return news;
        });
    };
    NewsService.prototype.getTotalPages = function (category) {
        if (!category) {
            return Observable_1.Observable.of(null);
        }
        var qstring = new http_1.URLSearchParams();
        qstring.append('category', category);
        return this.http.get(this.apiGetTotalPages, { search: qstring })
            .map(function (result) {
            return result.json();
        });
    };
    NewsService.prototype.setAsPrompt = function (news) {
        var qstring = new http_1.URLSearchParams();
        qstring.append('id', news.id);
        return this.http.get(this.apiPromptStatus, { search: qstring })
            .map(function (result) {
            return result.json();
        });
    };
    NewsService.prototype.resetPrompt = function (news) {
        var qstring = new http_1.URLSearchParams();
        qstring.append('id', news.id);
        return this.http.delete(this.apiPromptStatus, { search: qstring })
            .map(function (result) {
            return result.json();
        });
    };
    NewsService.prototype._correctUrl = function (sourceUrl, restUrl) {
        return sourceUrl.replace(/(https?\:\/\/\w*\.\w*)(.*)/g, '$1') + restUrl;
    };
    NewsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], NewsService);
    return NewsService;
}());
exports.NewsService = NewsService;
//# sourceMappingURL=news.service.js.map