"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/observable/of');
require('rxjs/add/operator/do');
require('rxjs/add/operator/delay');
var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
        this.apiLogin = '/api/account/login';
        this.apiLogout = '/api/account/logout';
        this.apiLoginStatus = '/api/account/checkloginstatus';
        this.isLoggedIn = false;
    }
    AuthService.prototype.login = function (name, password) {
        var _this = this;
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        var result = this.http.post(this.apiLogin, { "name": name, "password": password }, { headers: headers })
            .map(function (result) { return result.json(); })
            .do(function (res) {
            if (res) {
                _this.isLoggedIn = true;
            }
        }, function (error) {
            console.log(error);
        });
        return result;
    };
    AuthService.prototype.logout = function () {
        var _this = this;
        var result = this.http.get(this.apiLogout)
            .do(function (res) {
            _this.isLoggedIn = false;
        });
        return result;
    };
    /**
     * Fails if no auth cookies are valid
     */
    AuthService.prototype.CheckLoginStatus = function () {
        var _this = this;
        var result = this.http.get(this.apiLoginStatus)
            .do(function (res) {
            _this.isLoggedIn = true;
        }, function (error) {
            _this.isLoggedIn = false;
        });
        return result;
    };
    AuthService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map