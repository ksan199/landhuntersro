﻿import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

@Injectable()
export class AuthService {
    private apiLogin = '/api/account/login';
    private apiLogout = '/api/account/logout';
    private apiLoginStatus = '/api/account/checkloginstatus'
    isLoggedIn: boolean = false;

    redirectUrl: string;

    constructor(
        private http: Http
    ) {}

    login(name: string, password: string): Observable<boolean> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let result = this.http.post(this.apiLogin, { "name": name, "password": password}, { headers: headers })
            .map(result => result.json())
            .do(res => {
                if (res){
                    this.isLoggedIn = true;
                }
            }, error => {
                console.log(error);
            });

        return result;
    }

    logout(): Observable<any> {
        let result = this.http.get(this.apiLogout)
            .do(res => {
                this.isLoggedIn = false;
            });

        return result;
    }

    /**
     * Fails if no auth cookies are valid
     */
    CheckLoginStatus(): Observable<any> {
        let result =  this.http.get(this.apiLoginStatus)
            .do(res => {
                this.isLoggedIn = true;
            }, error => {
                this.isLoggedIn = false;
            });

        return result;
    }

}