import { Injectable } from '@angular/core';

@Injectable()
export class DateService {
    private mounthMap = {
        1 : "января",
        2 : "февраля",
        3 : "марта",
        4 : "апреля",
        5 : "мая",
        6 : "июня",
        7 : "июля",
        8 : "августа",
        9 : "сентября",
        10 : "октября",
        11 : "ноября",
        12 : "декабря"

    }

    constructor() { }

    public getCurrentTime(): string {
        var d = new Date(),
            day = d.getDate(),
            mounth = this.mounthMap[d.getMonth() + 1];
        return `${day} ${mounth}`;
    }
}