﻿import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

import { newsList, newsList_2, Details } from './news';
import { DetailNews } from './../models/detail-news.model';
import { ShortNews } from './../models/short-news.interface';

@Injectable()
export class NewsService {
    apiGetNews = '/api/news/get';
    apiGetTotalPages = '/api/news/gettotalpages';
    apiPromptStatus = '/api/news/promptstatus';
    apiGetDetails = '/api/news/details';

    constructor(private http: Http) { }

    getNewsDetails(id: string): Observable<DetailNews> {
        let qstring = new URLSearchParams();

        qstring.append('id', id);

        return this.http.get(this.apiGetDetails, { search: qstring })
            .map(result => {
                let rawDetails = result.json();
                let details = new DetailNews();

                details.id = rawDetails.id;

                if (!rawDetails.imgUrl || rawDetails.imgUrl == '') {
                    details.imgUrl = null;
                }
                else if (rawDetails.imgUrl.startsWith('/')) {
                    details.imgUrl = this._correctUrl(rawDetails.sourceUrl, rawDetails.imgUrl);
                }
                else {
                    details.imgUrl = rawDetails.imgUrl;
                }

                details.message = rawDetails.message;
                details.publishDate = rawDetails.publishDate;
                details.sourceUrl = rawDetails.sourceUrl;
                details.title = rawDetails.title;
                details.sourceName = rawDetails.sourceName;

                return details;
            });
    }
    
    /**
     * Returns a list of short news for current category and page
     */
    getNews(category: string, page: string, prompt = false): Observable<ShortNews[]> {
        let qstring = new URLSearchParams();

        qstring.append('category', category);
        qstring.append('page', page);

        return this.http.get(this.apiGetNews, { search: qstring })
            .map(result => {
                let rawNews = result.json();
                let news: ShortNews[] = [];
                
                for (let i of rawNews) {
                    let toPush = new ShortNews();
                    toPush.id = i.id;

                    if (!i.imgUrl || i.imgUrl == '') {
                        toPush.imgUrl = null;
                    }
                    else if (i.imgUrl.startsWith('/')) {
                        toPush.imgUrl = this._correctUrl(i.originalUrl, i.imgUrl);
                    }
                    else {
                        toPush.imgUrl = i.imgUrl;
                    }

                    if (i.prompt == null || (i.prompt == 'false')){
                        toPush.prompt = false;
                    } 
                    else {
                        toPush.prompt = true;
                    }
                    toPush.publishDate = i.publishDate;
                    if (i.shortMessage != null) toPush.shortMessage = i.shortMessage;
                    news.push(toPush);
                }

                return news;
            });   
    }

    getTotalPages(category: string): Observable<number> {
        if (!category) {
            return Observable.of(null);
        }
        let qstring = new URLSearchParams();
        qstring.append('category', category);
        return this.http.get(this.apiGetTotalPages, { search: qstring })
            .map(result => {
                return result.json();
            });      
    }

    setAsPrompt(news: ShortNews):Observable<ShortNews> {
        let qstring = new URLSearchParams();
        qstring.append('id', news.id);
        return this.http.get(this.apiPromptStatus, { search: qstring })
            .map(result => {
                return result.json();
            });
    }

    resetPrompt(news: ShortNews): Observable<ShortNews> {
        let qstring = new URLSearchParams();
        qstring.append('id', news.id);
        return this.http.delete(this.apiPromptStatus, {search: qstring})
            .map(result => {
                return result.json();
            });
    }

    private _correctUrl(sourceUrl: string, restUrl: string): string{
        return sourceUrl.replace(/(https?\:\/\/\w*\.\w*)(.*)/g,'$1') + restUrl;
    }
}