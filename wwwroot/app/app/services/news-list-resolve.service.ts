﻿import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { NewsService } from './news.service';
import { ShortNews } from './../models/short-news.interface';

@Injectable()
export class NewsListResolve implements Resolve<ShortNews[]> {
    constructor(private newsService: NewsService, private router: Router) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> | any {
        let id = route.params['id'],
            category = route.params['category'];
 
        return this.newsService.getNews(category, id);
    }
}