"use strict";
var short_news_interface_1 = require('./../models/short-news.interface');
var detail_news_model_1 = require('./../models/detail-news.model');
exports.newsList = [
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
];
exports.newsList_2 = [
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews(),
    new short_news_interface_1.ShortNews()
];
var moretext = "";
for (var i = 0; i < 40; i++)
    moretext += " It is really a lot of text here!";
exports.Details = new detail_news_model_1.DetailNews("Старт «доброго кузнеца». Сможет ли тверской Вадим Дешевкин удивить политологов?", moretext);
//# sourceMappingURL=news.js.map