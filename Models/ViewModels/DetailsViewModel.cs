namespace landhuntersro.Models.ViewModels
{
    public class DetailsViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string PublishDate { get; set; }
        public string ImgUrl { get; set; }
        public string SourceUrl { get; set; }
        public string SourceName { get; set; }
    }
}