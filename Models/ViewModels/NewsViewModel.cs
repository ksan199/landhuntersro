namespace landhuntersro.Models.ViewModels
{
    public class NewsViewModel
    {
        public int Id { get; set; }
        public string ImgUrl { get; set; }
        public string PublishDate { get; set; }
        public string Prompt { get; set; }
        public string ShortMessage { get; set; }
        public string OriginalUrl { get; set; }
    }
}