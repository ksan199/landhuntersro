namespace landhuntersrom.Models.HttpFromBodyModels 
{
    public class LoginModel
    {
        public string name { get; set; }
        public string password { get; set; }
    }
}