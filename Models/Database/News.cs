﻿using System;
using System.Collections.Generic;

namespace landhuntersro.Models.Database
{
    public partial class News
    {
        public News()
        {
            NewsCustomAttributes = new HashSet<NewsCustomAttributes>();
            PagesHtml = new HashSet<PagesHtml>();
            Paragraphs = new HashSet<Paragraphs>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Source { get; set; }
        public string Date { get; set; }
        public string IsPrompt { get; set; }

        public virtual ICollection<NewsCustomAttributes> NewsCustomAttributes { get; set; }
        public virtual ICollection<PagesHtml> PagesHtml { get; set; }
        public virtual ICollection<Paragraphs> Paragraphs { get; set; }
    }
}
