﻿using System;
using System.Collections.Generic;

namespace landhuntersro.Models.Database
{
    public partial class Paragraphs
    {
        public int Id { get; set; }
        public int? NewsId { get; set; }
        public string Text { get; set; }

        public virtual News News { get; set; }
    }
}
