﻿using System;
using System.Collections.Generic;

namespace landhuntersro.Models.Database
{
    public partial class OrderAttribute
    {
        public int Id { get; set; }
        public int? AttributeGroupId { get; set; }
        public string Name { get; set; }
        public string TName { get; set; }
        public string Value { get; set; }

        public virtual AttributeGroup AttributeGroup { get; set; }
    }
}
