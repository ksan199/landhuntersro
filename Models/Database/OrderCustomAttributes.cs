﻿using System;
using System.Collections.Generic;

namespace landhuntersro.Models.Database
{
    public partial class OrderCustomAttributes
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public string AttributeName { get; set; }
        public string AttributeValue { get; set; }

        public virtual Order Order { get; set; }
    }
}
