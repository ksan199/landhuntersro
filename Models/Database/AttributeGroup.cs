﻿using System;
using System.Collections.Generic;

namespace landhuntersro.Models.Database
{
    public partial class AttributeGroup
    {
        public AttributeGroup()
        {
            OrderAttribute = new HashSet<OrderAttribute>();
        }

        public int Id { get; set; }
        public int? OrderId { get; set; }
        public string Name { get; set; }
        public string TName { get; set; }

        public virtual ICollection<OrderAttribute> OrderAttribute { get; set; }
        public virtual Order Order { get; set; }
    }
}
