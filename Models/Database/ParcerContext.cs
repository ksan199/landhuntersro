﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using landhuntersro.Models.Database;

namespace landhuntersro.Models.Database
{
    public partial class ParcerContext : IdentityDbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            optionsBuilder.UseSqlServer(@"Server=KSAN\KSAN;Database=Parcer;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AttributeGroup>(entity =>
            {
                entity.ToTable("attribute_group");

                entity.HasIndex(e => new { e.Name, e.OrderId })
                    .HasName("UQ__attribut__F684B93900F0BACC")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(450)");

                entity.Property(e => e.OrderId)
                    .IsRequired()
                    .HasColumnName("order_id");

                entity.Property(e => e.TName)
                    .HasColumnName("t_name")
                    .HasColumnType("varchar(max)");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.AttributeGroup)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK__attribute__order__34C8D9D1");
            });

            modelBuilder.Entity<News>(entity =>
            {
                entity.ToTable("news");

                entity.HasIndex(e => e.Url)
                    .HasName("UQ__news__DD7784177FA0F241")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.IsPrompt)
                    .HasColumnName("is_prompt")
                    .HasColumnType("varchar(max)")
                    .HasDefaultValue("false");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnName("url")
                    .HasColumnType("varchar(450)");
            });

            modelBuilder.Entity<NewsCustomAttributes>(entity =>
            {
                entity.ToTable("news_custom_attributes");

                entity.HasIndex(e => new { e.NewsId, e.AttributeName })
                    .HasName("UQ__news_cus__B86B68470FB65D4B")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AttributeName)
                    .IsRequired()
                    .HasColumnName("attribute_name")
                    .HasColumnType("varchar(450)");

                entity.Property(e => e.AttributeValue)
                    .HasColumnName("attribute_value")
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.NewsId)
                    .IsRequired()
                    .HasColumnName("news_id");

                entity.HasOne(d => d.News)
                    .WithMany(p => p.NewsCustomAttributes)
                    .HasForeignKey(d => d.NewsId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK__news_cust__news___2D27B809");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("order");

                entity.HasIndex(e => e.Url)
                    .HasName("UQ__order__DD778417F5FD2BF5")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnName("url")
                    .HasColumnType("varchar(450)");
            });

            modelBuilder.Entity<OrderAttribute>(entity =>
            {
                entity.ToTable("order_attribute");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AttributeGroupId).HasColumnName("attribute_group_id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.TName)
                    .HasColumnName("t_name")
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("varchar(max)");

                entity.HasOne(d => d.AttributeGroup)
                    .WithMany(p => p.OrderAttribute)
                    .HasForeignKey(d => d.AttributeGroupId)
                    .HasConstraintName("FK__order_att__attri__3A81B327");
            });

            modelBuilder.Entity<OrderCustomAttributes>(entity =>
            {
                entity.ToTable("order_custom_attributes");

                entity.HasIndex(e => new { e.OrderId, e.AttributeName })
                    .HasName("UQ__order_cu__B215C6B6580084E7")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AttributeName)
                    .IsRequired()
                    .HasColumnName("attribute_name")
                    .HasColumnType("varchar(450)");

                entity.Property(e => e.AttributeValue)
                    .HasColumnName("attribute_value")
                    .HasColumnType("varchar(max)");

                entity.Property(e => e.OrderId)
                    .IsRequired()
                    .HasColumnName("order_id");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderCustomAttributes)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK__order_cus__order__30F848ED");
            });

            modelBuilder.Entity<PagesHtml>(entity =>
            {
                entity.HasKey(e => new { e.NewsId, e.Html })
                    .HasName("PK__pages_ht__6820299ACE97369C");

                entity.ToTable("pages_html");

                entity.Property(e => e.NewsId).HasColumnName("news_id");

                entity.Property(e => e.Html)
                    .HasColumnName("html")
                    .HasColumnType("varchar(450)");

                entity.HasOne(d => d.News)
                    .WithMany(p => p.PagesHtml)
                    .HasForeignKey(d => d.NewsId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK__pages_htm__news___29572725");
            });

            modelBuilder.Entity<Paragraphs>(entity =>
            {
                entity.ToTable("paragraphs");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.NewsId).HasColumnName("news_id");

                entity.Property(e => e.Text)
                    .HasColumnName("text")
                    .HasColumnType("varchar(max)");

                entity.HasOne(d => d.News)
                    .WithMany(p => p.Paragraphs)
                    .HasForeignKey(d => d.NewsId)
                    .HasConstraintName("FK__paragraph__news___37A5467C");
            });

            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<AttributeGroup> AttributeGroup { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<NewsCustomAttributes> NewsCustomAttributes { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderAttribute> OrderAttribute { get; set; }
        public virtual DbSet<OrderCustomAttributes> OrderCustomAttributes { get; set; }
        public virtual DbSet<PagesHtml> PagesHtml { get; set; }
        public virtual DbSet<Paragraphs> Paragraphs { get; set; }
        public virtual DbSet<ApplicationUser> ApplicationUser { get; set; }
    }
}