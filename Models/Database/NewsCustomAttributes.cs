﻿using System;
using System.Collections.Generic;

namespace landhuntersro.Models.Database
{
    public partial class NewsCustomAttributes
    {
        public int Id { get; set; }
        public int? NewsId { get; set; }
        public string AttributeName { get; set; }
        public string AttributeValue { get; set; }

        public virtual News News { get; set; }
    }
}
