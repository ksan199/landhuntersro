﻿using System;
using System.Collections.Generic;

namespace landhuntersro.Models.Database
{
    public partial class Order
    {
        public Order()
        {
            AttributeGroup = new HashSet<AttributeGroup>();
            OrderCustomAttributes = new HashSet<OrderCustomAttributes>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Source { get; set; }

        public virtual ICollection<AttributeGroup> AttributeGroup { get; set; }
        public virtual ICollection<OrderCustomAttributes> OrderCustomAttributes { get; set; }
    }
}
