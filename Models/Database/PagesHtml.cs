﻿using System;
using System.Collections.Generic;

namespace landhuntersro.Models.Database
{
    public partial class PagesHtml
    {
        public int NewsId { get; set; }
        public string Html { get; set; }

        public virtual News News { get; set; }
    }
}
