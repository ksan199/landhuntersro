﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using landhuntersro.Models.Database;
using landhuntersro.Models.ViewModels;
using LandhunterSro.Services;
using Microsoft.AspNetCore.Mvc;

namespace LandhunterSro.Controllers
{
    [Route("api/[controller]/[action]")]
    public class NewsController : Controller
    {
        public RepositoryService _repository { get; set; }
        public NewsController(RepositoryService repository)
        {
            _repository = repository;
        }

        // GET api/values
        [HttpGet]
        public  async Task<IEnumerable<NewsViewModel>> Get(string category, string page)
        {
            
            var news = await _repository.GetNews(category, page);
            
            return news;
        }

        public async Task<DetailsViewModel> Details(int id)
        {
            return await _repository.GetDetails(id);
        }

        [HttpGet]
        public async Task<int> GetTotalPages(string category)
        {
            return await _repository.GetTotalPages(category);
        }

        // GET api/values/5
        [HttpGet]
        public bool PromptStatus(int id)
        {
            return _repository.SetAsPrompt(id).Result;
        }

        [HttpDelete]
        [ActionName("PromptStatus")]
        public bool PromptStatusDelete(int id)
        {
            return _repository.DeletePrompt(id).Result;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
