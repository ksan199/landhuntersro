using System.Threading.Tasks;
using landhuntersro.Models.Database;
using landhuntersrom.Models.HttpFromBodyModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LandhunterSro.Controllers 
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class AccountController: Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager
        )
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<bool> Login([FromBody] LoginModel data)
        {
            var result = await _signInManager.PasswordSignInAsync(data.name, data.password, true, false);
            return result.Succeeded;
        }

        public async void Logout()
        {
            await _signInManager.SignOutAsync();
        }

        public void CheckLoginStatus()
        {
            
        }
    }
}