using landhuntersro.Models.Database;
using LandhunterSro.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace LandhunterSro.Services
{
    public class SeedDataService: ISeedDataService
    {
        public readonly ParcerContext _context;
        public readonly UserManager<ApplicationUser> _userManager;
        public SeedDataService(ParcerContext context, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        public async void SeedData() 
        {
  
            _context.Database.Migrate();
            
            var result = await _userManager.FindByEmailAsync("techno199@live.ru");

            if (result == null)
            {
                var newUser = await _userManager.CreateAsync(
                    new ApplicationUser() {
                        UserName = "ksan199",
                        Email = "techno199@live.ru"
                    },
                    "Hk6+dRst"
                );
            }
        }
    }
}