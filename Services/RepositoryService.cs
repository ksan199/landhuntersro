using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using landhuntersro.Models.Database;
using landhuntersro.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace LandhunterSro.Services
{
    public class RepositoryService
    {
        public ParcerContext _context { get; set; }
        public RepositoryService(ParcerContext context)
        {
            _context = context;
        }

        public async Task<DetailsViewModel> GetDetails(int id)
        {
            var dataFromNewsTable = await _context.News.FirstAsync(e => e.Id == id);
            var paragraphsList = await _context.Paragraphs.Where(e => e.NewsId == id).ToListAsync();
            var ImgUrl = _context
                .NewsCustomAttributes
                .SingleAsync(e => e.NewsId == id && e.AttributeName == "preview")
                .Result.AttributeValue;
            string fullText = "", sourceName ="";
            foreach (var p in paragraphsList)
            {
                fullText += "<p>" + p.Text + "</p>";
            }

            try 
            {
                sourceName = _SourceMap[dataFromNewsTable.Source];
            }
            catch
            {
                sourceName = "Ссылка";
            }
            var details = new DetailsViewModel() {
                Id = id,
                Title = dataFromNewsTable.Name,
                Message = fullText,
                ImgUrl = ImgUrl,
                SourceUrl = dataFromNewsTable.Url,
                SourceName = sourceName
            };

            return details;
        }

        public async Task<IEnumerable<NewsViewModel>> GetNews(string category, string page)
        {
            var pageInt = System.Convert.ToInt32(page);
            try 
            {
                var news = await _context
                    .News
                    .Where(e => _SourceCondition(e, category))
                    .Skip(RepositoryService.NEWS_AT_SINGLE_PAGE*pageInt - RepositoryService.NEWS_AT_SINGLE_PAGE)
                    .Take(RepositoryService.NEWS_AT_SINGLE_PAGE)
                    .ToListAsync();
                var viewModels = new List<NewsViewModel>();

                foreach (var n in news)
                {
                    var vm = new NewsViewModel();
                    vm.Id = n.Id;
                    vm.ImgUrl = _context
                        .NewsCustomAttributes
                        .SingleAsync(e => e.NewsId == n.Id && e.AttributeName == "preview")
                        .Result
                        .AttributeValue;

                    vm.Prompt = n.IsPrompt;
                    vm.ShortMessage = n.Name;
                    vm.PublishDate = n.Date;
                    vm.OriginalUrl = n.Url;
                    viewModels.Add(vm);
                }
        
                return viewModels;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<int> GetTotalPages(string category)
        {
            try
            {
                var newsCount = await _context
                    .News
                    .Where(e => _SourceCondition(e, category))
                    .CountAsync();
                var pages = (double)newsCount / (double)NEWS_AT_SINGLE_PAGE;
                return (int)System.Math.Ceiling(pages);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public async Task<bool> SetAsPrompt(int id)
        {
            var result = await _context
                .News
                .Where(e => e.Id == id)
                .FirstAsync();

            result.IsPrompt = "True";
            var isSaved = _context.SaveChanges(true);
            return isSaved > 0;
        }

        public async Task<bool> DeletePrompt(int id)
        {
            var result = await _context
                .News
                .Where(e => e.Id == id)
                .FirstAsync();

            result.IsPrompt = null;
            var isSaved = _context.SaveChanges(true);
            return isSaved > 0;
        }

        #region Helpers
        public Dictionary<string, string> SourceDict { get; set; }
        public const int NEWS_AT_SINGLE_PAGE = 20;
        public const string CLIENT_SOURCE_NAME_ALL_NEWS = "all";
        public const string CLIENT_SOURCE_NAME_PROMPT_NEWS = "prompt";
        public const string CLIENT_SOURCE_NAME_RBK_1 = "rbc_ru";
        public const string CLIENT_SOURCE_NAME_RBK_2 = "reality_rbc_ru";
        //Returns true if selected news satisfy category
        private bool _SourceCondition(News n, string category) 
        {
            if (category == CLIENT_SOURCE_NAME_ALL_NEWS)
            {
                return true;
            }
            else if (category == CLIENT_SOURCE_NAME_PROMPT_NEWS)
            {
                return !(n.IsPrompt == null) && !(n.IsPrompt.ToLower() == "false");
            }
            else if (category == CLIENT_SOURCE_NAME_RBK_1)
            {
                return (n.Source == CLIENT_SOURCE_NAME_RBK_1 || n.Source == CLIENT_SOURCE_NAME_RBK_2);
            }
            else
            {
                return n.Source == category;
            }
        }

        // Maps source names from db to real ones
        public Dictionary<string, string> _SourceMap = new Dictionary<string, string>
        {
            { "rbc_ru", "РБК" },
            { "reality_rbc_ru", "РБК" },
            { "rosreestr_ru_press", "Росреестр" },
            { "riarealty_ru", "РИА" }
        };
        #endregion
    }
}