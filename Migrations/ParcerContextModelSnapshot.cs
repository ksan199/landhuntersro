﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using landhuntersro.Models.Database;

namespace landhuntersro.Migrations
{
    [DbContext(typeof(ParcerContext))]
    partial class ParcerContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("landhuntersro.Models.Database.AttributeGroup", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasColumnType("varchar(450)");

                    b.Property<int?>("OrderId")
                        .IsRequired()
                        .HasColumnName("order_id");

                    b.Property<string>("TName")
                        .HasColumnName("t_name")
                        .HasColumnType("varchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("OrderId");

                    b.HasIndex("Name", "OrderId")
                        .IsUnique()
                        .HasName("UQ__attribut__F684B93900F0BACC");

                    b.ToTable("attribute_group");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.News", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Date")
                        .HasColumnName("date")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("IsPrompt")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("is_prompt")
                        .HasColumnType("varchar(max)")
                        .HasDefaultValue("false");

                    b.Property<string>("Name")
                        .HasColumnName("name")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("Source")
                        .HasColumnName("source")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("Url")
                        .IsRequired()
                        .HasColumnName("url")
                        .HasColumnType("varchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("Url")
                        .IsUnique()
                        .HasName("UQ__news__DD7784177FA0F241");

                    b.ToTable("news");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.NewsCustomAttributes", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("AttributeName")
                        .IsRequired()
                        .HasColumnName("attribute_name")
                        .HasColumnType("varchar(450)");

                    b.Property<string>("AttributeValue")
                        .HasColumnName("attribute_value")
                        .HasColumnType("varchar(max)");

                    b.Property<int?>("NewsId")
                        .IsRequired()
                        .HasColumnName("news_id");

                    b.HasKey("Id");

                    b.HasIndex("NewsId");

                    b.HasIndex("NewsId", "AttributeName")
                        .IsUnique()
                        .HasName("UQ__news_cus__B86B68470FB65D4B");

                    b.ToTable("news_custom_attributes");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Name")
                        .HasColumnName("name")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("Source")
                        .HasColumnName("source")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("Url")
                        .IsRequired()
                        .HasColumnName("url")
                        .HasColumnType("varchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("Url")
                        .IsUnique()
                        .HasName("UQ__order__DD778417F5FD2BF5");

                    b.ToTable("order");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.OrderAttribute", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<int?>("AttributeGroupId")
                        .HasColumnName("attribute_group_id");

                    b.Property<string>("Name")
                        .HasColumnName("name")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("TName")
                        .HasColumnName("t_name")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("Value")
                        .HasColumnName("value")
                        .HasColumnType("varchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("AttributeGroupId");

                    b.ToTable("order_attribute");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.OrderCustomAttributes", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("AttributeName")
                        .IsRequired()
                        .HasColumnName("attribute_name")
                        .HasColumnType("varchar(450)");

                    b.Property<string>("AttributeValue")
                        .HasColumnName("attribute_value")
                        .HasColumnType("varchar(max)");

                    b.Property<int?>("OrderId")
                        .IsRequired()
                        .HasColumnName("order_id");

                    b.HasKey("Id");

                    b.HasIndex("OrderId");

                    b.HasIndex("OrderId", "AttributeName")
                        .IsUnique()
                        .HasName("UQ__order_cu__B215C6B6580084E7");

                    b.ToTable("order_custom_attributes");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.PagesHtml", b =>
                {
                    b.Property<int>("NewsId")
                        .HasColumnName("news_id");

                    b.Property<string>("Html")
                        .HasColumnName("html")
                        .HasColumnType("varchar(450)");

                    b.HasKey("NewsId", "Html")
                        .HasName("PK__pages_ht__6820299ACE97369C");

                    b.HasIndex("NewsId");

                    b.ToTable("pages_html");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.Paragraphs", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<int?>("NewsId")
                        .HasColumnName("news_id");

                    b.Property<string>("Text")
                        .HasColumnName("text")
                        .HasColumnType("varchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("NewsId");

                    b.ToTable("paragraphs");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");

                    b.HasDiscriminator<string>("Discriminator").HasValue("IdentityUser");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.ApplicationUser", b =>
                {
                    b.HasBaseType("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUser");


                    b.ToTable("ApplicationUser");

                    b.HasDiscriminator().HasValue("ApplicationUser");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.AttributeGroup", b =>
                {
                    b.HasOne("landhuntersro.Models.Database.Order", "Order")
                        .WithMany("AttributeGroup")
                        .HasForeignKey("OrderId")
                        .HasConstraintName("FK__attribute__order__34C8D9D1");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.NewsCustomAttributes", b =>
                {
                    b.HasOne("landhuntersro.Models.Database.News", "News")
                        .WithMany("NewsCustomAttributes")
                        .HasForeignKey("NewsId")
                        .HasConstraintName("FK__news_cust__news___2D27B809");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.OrderAttribute", b =>
                {
                    b.HasOne("landhuntersro.Models.Database.AttributeGroup", "AttributeGroup")
                        .WithMany("OrderAttribute")
                        .HasForeignKey("AttributeGroupId")
                        .HasConstraintName("FK__order_att__attri__3A81B327");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.OrderCustomAttributes", b =>
                {
                    b.HasOne("landhuntersro.Models.Database.Order", "Order")
                        .WithMany("OrderCustomAttributes")
                        .HasForeignKey("OrderId")
                        .HasConstraintName("FK__order_cus__order__30F848ED");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.PagesHtml", b =>
                {
                    b.HasOne("landhuntersro.Models.Database.News", "News")
                        .WithMany("PagesHtml")
                        .HasForeignKey("NewsId")
                        .HasConstraintName("FK__pages_htm__news___29572725");
                });

            modelBuilder.Entity("landhuntersro.Models.Database.Paragraphs", b =>
                {
                    b.HasOne("landhuntersro.Models.Database.News", "News")
                        .WithMany("Paragraphs")
                        .HasForeignKey("NewsId")
                        .HasConstraintName("FK__paragraph__news___37A5467C");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
