namespace LandhunterSro.Interfaces
{
    public interface ISeedDataService
    {
        void SeedData(); 
    }
}